#include <cstdio>
#include <vector>
#include "Headers/UDPClient.hpp"
#include "Headers/UDPServer.hpp"
#include "Headers/ConcurrentFileLogger.hpp"
#include <atomic>
#include "Headers/MessageCreationSend.hpp"
#include "Headers/ConcurrentMap.hpp"
#include "Headers/MessageRetry.hpp"
#include "Headers/FutureRun.hpp"

const int DATA_LIMIT_SIZE = 3000;
const int DEFAULT_ACK_WAIT_TIME = 5000;
const int HEARTBEAT_STATUS_INTERVAL_IN_MS = 10000;
const std::string LOOPBACK_ADDRESS = "127.0.0.1";

std::mutex printToConsoleMutexLock;
std::atomic<int> receivedMessageCount;
ConcurrentFileLogger logger("MissionExecModuleLog.txt");
std::atomic<bool> inManualMode;
std::atomic<bool> inKillSwitchMode;

ConcurrentQueue<std::tuple<float, float>> coordinatesFromOperator;

// Key: MsgChecksum, Value: Number of responses waiting for
ConcurrentMap<uint16_t, int> messagesToBeReturnedRetryCount;

// Key: MsgChecksum, Value: messageByteData
ConcurrentMap<uint16_t, uint8_t[]> messagesToBeReturnedByteData;

[[noreturn]] void HandleIncomingData(UDPServer& serverConnection, ConcurrentQueue<std::vector<char>>& dataReceived) {
    while(true) {
        char receivedBuffer[DATA_LIMIT_SIZE];
        int receivedByteCount = 0;

        //printf("Waiting for local data: \n");
        serverConnection.GetDatagram(std::ref(receivedByteCount), receivedBuffer, DATA_LIMIT_SIZE);
        //printf("Received local data: %s\n", receivedBuffer);

        dataReceived.push(CharArrayToVector(receivedBuffer, receivedByteCount));
    }
}

[[noreturn]] void SendDataToServerConnection(UDPClient& clientConnection, ConcurrentQueue<std::vector<char>>& dataQueue) {
    while(true) {
        std::vector<char> dataToSend;
        char buffer[DATA_LIMIT_SIZE];

        dataQueue.pop(dataToSend);
        VectorToCharArray(dataToSend, buffer, DATA_LIMIT_SIZE);

        if (!clientConnection.SentDatagramToServerSuccessfully(buffer, dataToSend.size())) {
            logger.WriteToFile("Error sending data\n");
        }
    }
}

void CheckIfExpectedMsgReceived(uint16_t expectedMsgChecksum) {
    int currentMsgWaitCount = messagesToBeReturnedRetryCount.GetElementAt(expectedMsgChecksum);
    if(currentMsgWaitCount == 0) {
        // Expected message was received
        RemoveMsgToBeWaitedOn(messagesToBeReturnedRetryCount, expectedMsgChecksum);
    } else {
        // Expected message was not received
        DecrementMsgWaitResponse(messagesToBeReturnedRetryCount, expectedMsgChecksum);
        FutureRun waitForMessage(5000, true, &CheckIfExpectedMsgReceived, expectedMsgChecksum);
    }
}

void AddFutureWaitForMessage(uint16_t expectedMsgChecksum) {
    AddNewMsgToWaitForResponse(messagesToBeReturnedRetryCount, expectedMsgChecksum);
    FutureRun waitForMessage(DEFAULT_ACK_WAIT_TIME, true, &CheckIfExpectedMsgReceived, expectedMsgChecksum);
}

void DidReceiveExpectedMessage(uint16_t expectedMsgChecksum) {
    SetKeyToZero(messagesToBeReturnedRetryCount, expectedMsgChecksum);
}

void ParseCmdInt(mavlink_command_int_t mavCmdInt, ConcurrentQueue<std::vector<char>>& outgoingData) {
    uint16_t msgChecksum = 0;
    float latitude = 0;
    float longitude = 0;

    switch (mavCmdInt.command) {

        case MAV_CMD_NAV_WAYPOINT:
            latitude = mavCmdInt.param1;
            longitude = mavCmdInt.param2;
            // Store navigate to coordinate point from operator
            coordinatesFromOperator.push(std::tuple<float, float>(latitude, longitude));
            // Pass along location to navigate to path planning module
            msgChecksum = SendWaypointLocation(outgoingData, MissionExecutive, CentralExecutive,
                                               latitude, longitude, DATA_LIMIT_SIZE, logger);
            AddFutureWaitForMessage(msgChecksum);
            break;

        case MAV_CMD_DO_FLIGHTTERMINATION:
            // Received kill switch status update
            inKillSwitchMode = (mavCmdInt.param1 == 1);
            // Pass on to central exec
            msgChecksum = SendKillSwitchStatus(outgoingData, inKillSwitchMode, MissionExecutive,
                    CentralExecutive, DATA_LIMIT_SIZE, logger);
            AddFutureWaitForMessage(msgChecksum);
            break;

        case MAV_CMD_DO_PAUSE_CONTINUE:
            // Received go/stop cmd for mission, pass on central exec
            msgChecksum = SendStopGoCommand(outgoingData, (mavCmdInt.param1 == 1), MissionExecutive, CentralExecutive,
                    DATA_LIMIT_SIZE, logger);
            AddFutureWaitForMessage(msgChecksum);
            break;

        case MAV_CMD_DO_SET_MODE:
            inManualMode = (mavCmdInt.param1 == MAV_MODE_MANUAL_ARMED);
            msgChecksum = SendManualModeStatus(outgoingData, inManualMode, MissionExecutive,
                    CentralExecutive, DATA_LIMIT_SIZE, logger);
            AddFutureWaitForMessage(msgChecksum);
            break;

        default:
            // Log unknown message with time ********************
            logger.WriteToFile("Invalid CMD INT MAVLink Data Received");
            break;
    }
}

void ParseCmdLong(__mavlink_command_long_t mavCmdLong, ConcurrentQueue<std::vector<char>>& outgoingData) {

}

[[noreturn]] void ParseReceivedData(ConcurrentQueue<std::vector<char>>& messagesToParse,
                                    ConcurrentQueue<std::vector<char>>& outgoingData) {
    while(true) {
        char receivedBuffer[DATA_LIMIT_SIZE];
        uint8_t sendBuffer[DATA_LIMIT_SIZE];
        int receivedByteCount = 0;
        std::vector<char> dataToBeSent;

        messagesToParse.pop(dataToBeSent);
        receivedByteCount = dataToBeSent.size();
        VectorToCharArray(dataToBeSent, receivedBuffer, DATA_LIMIT_SIZE);

        mavlink_message_t msg;
        mavlink_status_t status;
        unsigned int firstCharInBuffer = -1;
        bool dataWasCorrectlyParsed = false;

        for (int bufferIndex = 0; bufferIndex < receivedByteCount; ++bufferIndex) {
            firstCharInBuffer = dataToBeSent[bufferIndex];
            //printf("%02x ", (unsigned char) firstCharInBuffer);

            if (mavlink_parse_char(MAVLINK_COMM_0, dataToBeSent[bufferIndex], &msg, &status)) {
                uint8_t buffer[DATA_LIMIT_SIZE];
                logger.WriteToFile(MAVLinkMsgToStringStream(msg, true));
                dataWasCorrectlyParsed = true;
                int sizeInBytes = 0;
                uint16_t msgChecksum = 0;

                switch (msg.msgid) {
                    case MAVLINK_MSG_ID_HEARTBEAT:
                        break;

                    case MAVLINK_MSG_ID_COMMAND_INT:
                        mavlink_command_int_t mavCmdInt;
                        mavlink_msg_command_int_decode(&msg, &mavCmdInt);
                        ParseCmdInt(mavCmdInt, std::ref(outgoingData));
                        break;

                    case MAVLINK_MSG_ID_COMMAND_LONG:
                        __mavlink_command_long_t mavCmdLong;
                        mavlink_msg_command_long_decode(&msg, &mavCmdLong);
                        ParseCmdLong(mavCmdLong, outgoingData);
                        break;

                    case MAVLINK_MSG_ID_LANDING_TARGET:
                        // Received remaining distance to target location
                        mavlink_landing_target_t distanceToTargetMessage;
                        mavlink_msg_landing_target_decode(&msg, &distanceToTargetMessage);
                        // Forward to GUI
                        // From path planning module, send to mission exec and motor control
                        msgChecksum = SendDistanceToLocationMsg(outgoingData, MissionExecutive,
                                GUI, DATA_LIMIT_SIZE, distanceToTargetMessage, logger);
                        AddFutureWaitForMessage(msgChecksum);
                        break;

                    case MAVLINK_MSG_ID_MISSION_CURRENT:
                        // Received IED detection
                        mavlink_mission_current_t iedDetection;
                        mavlink_msg_mission_current_decode(&msg, &iedDetection);

                        // Notify operator
                        msgChecksum = SendIEDDetectionMessage(outgoingData, MissionExecutive,
                                                              GUI,iedDetection.seq == 1,
                                                              DATA_LIMIT_SIZE, logger);

                        // Put in manual mode
                        if(!inManualMode && iedDetection.seq == 1) {
                            inManualMode = true;
                        }
                        msgChecksum = SendManualModeStatus(outgoingData, inManualMode, MissionExecutive,
                                                           GUI, DATA_LIMIT_SIZE, logger);
                        break;

                    case MAVLINK_MSG_ID_MISSION_ITEM_REACHED:
                        // Received at dest msg from path planning
                        // Pass indication to gui
                        msgChecksum = SendLocationReachedMessage(outgoingData, MissionExecutive,
                                GUI,true, DATA_LIMIT_SIZE, logger);
                        AddFutureWaitForMessage(msgChecksum);

                        // Set in manual mode
                        inManualMode = true;
                        msgChecksum = SendManualModeStatus(outgoingData, inManualMode, MissionExecutive,
                                                           GUI, DATA_LIMIT_SIZE, logger);
                        AddFutureWaitForMessage(msgChecksum);

                        // Remove coordinate from list
                        if(coordinatesFromOperator.size() > 0) {
                            coordinatesFromOperator.pop();
                        }
                        break;

                    case MAVLINK_MSG_ID_COLLISION:
                        // Received ultrasonic detection
                        if(!inManualMode) {
                            inManualMode = true;
                            msgChecksum = SendManualModeStatus(outgoingData, inManualMode, MissionExecutive,
                                                               GUI, DATA_LIMIT_SIZE, logger);
                        }

                        // Send to GUI to notify operator
                        msgChecksum = SendCollisionMessage(outgoingData, true,
                                                           MissionExecutive,GUI, DATA_LIMIT_SIZE,
                                                           logger);
                        AddFutureWaitForMessage(msgChecksum);
                        break;

                    default:
                        // Log unknown message with time ********************
                        logger.WriteToFile("Invalid MAVLink Data Received\n");
                        break;
                }
            }
        }
        if(!dataWasCorrectlyParsed) {
            logger.WriteToFile("Invalid Data Received\n");
        }
    }
}

[[noreturn]] void SendHeartbeatStatusMessage(ConcurrentQueue<std::vector<char>>& outgoingData) {
    while(true) {
        /* Send Heartbeat */
        uint8_t buf[DATA_LIMIT_SIZE];
        mavlink_message_t msg;
        uint16_t len;
        mavlink_msg_heartbeat_pack(MissionExecutive, CCSCommunication,
                &msg, MAV_TYPE_GROUND_ROVER, MAV_AUTOPILOT_GENERIC,
                MAV_MODE_GUIDED_ARMED, 0, MAV_STATE_ACTIVE);
        len = mavlink_msg_to_send_buffer(buf, &msg);

        std::vector<char> dataToSend = CharArrayToVector((char*)buf, len);
        outgoingData.push(dataToSend);
        logger.WriteToFile(MAVLinkMsgToStringStream(msg, false));

        // Sleep for X seconds
        Sleep(HEARTBEAT_STATUS_INTERVAL_IN_MS);
    }
}

int main(int argc, char *argv[])
{
    // Data structures for handling local data
    receivedMessageCount = 0;
    inManualMode = false;
    inKillSwitchMode = false;
    ConcurrentQueue<std::vector<char>> outgoingData;
    ConcurrentQueue<std::vector<char>> incomingDataToParse;

    // UDP data connections
    UDPClient connectionToCCSComModule(CCSCommunicationModule, LOOPBACK_ADDRESS);
    UDPServer receiverFromServerModule(MissionExecutiveModule, LOOPBACK_ADDRESS);

    // Data connections
    std::thread sendData(SendDataToServerConnection, std::ref(connectionToCCSComModule), std::ref(outgoingData));
    std::thread handleIncomingData(HandleIncomingData, std::ref(receiverFromServerModule),
            std::ref(incomingDataToParse));

    // Module logic execution
    std::thread handleReceivedDataMessages(ParseReceivedData, std::ref(incomingDataToParse),
            std::ref(outgoingData));
    std::thread sendHeartbeatStatus(SendHeartbeatStatusMessage, std::ref(outgoingData));

    printf("Mission Executive Module Executing\n");

    //******************FOR TESTING*********************************************************************

    char loopControl = '\n';
    //do {
        //PrintToScreen("Hit Enter to continue: ");
        //loopControl = getchar();
    //} while(loopControl == '\n');

    //SendManualModeStatus(outgoingData, true, MissionExecutive, MissionExecutive, DATA_LIMIT_SIZE, logger);

    //******************FOR TESTING*********************************************************************

    sendData.join();
    handleIncomingData.join();
    handleReceivedDataMessages.join();
    sendHeartbeatStatus.join();

    return 0;
}