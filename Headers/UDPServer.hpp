#ifndef UDPENCRYPTEDSERVER_UDPSERVER_HPP
#define UDPENCRYPTEDSERVER_UDPSERVER_HPP

#include <memory.h>
#include <cstdio>
#include <cstdlib>
#include <string>

#include <winsock2.h>
#include <stdio.h>
#include "Ws2tcpip.h"

#pragma comment(lib, "Ws2_32.lib")

class UDPServer {
private:
    int serverPort;
    std::string serverAddress;

    WSADATA wsaData;
    SOCKET socketS;
    struct sockaddr_in local;
    struct sockaddr_in from;
    int fromlen = 0;

    void InitializeServer() {
        WSAStartup(MAKEWORD(2, 2), &wsaData);

        fromlen = sizeof(from);
        local.sin_family = AF_INET;
        local.sin_port = htons(serverPort);
        local.sin_addr.s_addr = INADDR_ANY;

        socketS = socket(AF_INET, SOCK_DGRAM, 0);
        bind(socketS, (sockaddr*)&local, sizeof(local));
    }

public:
    UDPServer(int port, std::string address) {
        serverPort = port;
        serverAddress = address;

        InitializeServer();
    }

    void GetDatagram(int& receivedByteCount, char* receivedBuffer, int serverDataLimitSize) {
        int errorCode = 0; // 0 is no error
        errorCode = recvfrom(socketS, receivedBuffer, serverDataLimitSize, 0, (sockaddr*)&from, &fromlen);
        if(errorCode == SOCKET_ERROR) {
            printf("UDP Server Get Error on %d\n", serverPort);
        } else {
            // If no error, receive returns byte count from packet
            receivedByteCount = errorCode;
        }
    }

    void SendDatagram(int sendByteCount, char* sendBuffer) {
        sendto(socketS, sendBuffer, sizeof(sendBuffer), 0, (sockaddr*)&from, fromlen);
    }


};

#endif //UDPENCRYPTEDSERVER_UDPSERVER_HPP
