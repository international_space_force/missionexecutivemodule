#ifndef CCSCOMMUNICATIONMODULE_CONCURRENTFILELOGGER_HPP
#define CCSCOMMUNICATIONMODULE_CONCURRENTFILELOGGER_HPP

#include <string>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <mutex>

class ConcurrentFileLogger {
private:
    std::string logFileName;
    std::fstream loggingFile;
    std::mutex logFileLock;

    void OpenFile() {
        loggingFile.open(logFileName, std::ios::in | std::ios::out | std::ios::app);
    }

    void LogTime() {
        auto t = std::time(nullptr);
        auto tm = *std::localtime(&t);
        loggingFile << std::put_time(&tm, "[%d-%m-%Y %H:%M:%S]") << std::endl;
    }

public:

    ConcurrentFileLogger() {
        logFileName = "logfile";
        OpenFile();
    }

    ConcurrentFileLogger(std::string filename) {
        logFileName = filename;
        OpenFile();
    }

    ~ConcurrentFileLogger() {
        loggingFile.close();
    }

    void WriteToFile(char* fileData) {
        std::lock_guard<std::mutex>lck(logFileLock);
        LogTime();
        if(loggingFile.is_open()) {
            loggingFile << fileData << std::endl;
        }
    }

    void WriteToFile(std::stringstream fileData) {
        std::lock_guard<std::mutex>lck(logFileLock);
        LogTime();
        if(loggingFile.is_open()) {
            loggingFile << fileData.str() << std::endl;
        }
    }

};

#endif //CCSCOMMUNICATIONMODULE_CONCURRENTFILELOGGER_HPP
